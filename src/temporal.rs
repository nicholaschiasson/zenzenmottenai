use piston_window::*;

use crate::input;

pub trait Temporal {
	fn update(&mut self, args: &UpdateArgs, input: Option<input::Delta>);
}
