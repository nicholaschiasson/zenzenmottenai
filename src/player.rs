use piston_window::*;
use vecmath::vec2_normalized;

use crate::controllable::{Controllable, Order};
use crate::input;
use crate::temporal::Temporal;

pub struct Player<T: Controllable> {
	character: T
}

impl<T: Controllable> Player<T> {
	pub fn new(controllable: T) -> Self {
		Player {
			character: controllable
		}
	}

	pub fn get_character(&mut self) -> &mut T {
		&mut self.character
	}
}

impl<T: Controllable> Temporal for Player<T> {
	fn update(&mut self, _args: &UpdateArgs, input: Option<input::Delta>) {
		if let Some(i) = input {
			if i.button_down(Button::Keyboard(Key::W)) {
				self.character.obey_order(Order::Move(vec2_normalized([0.0, -1.0])));
			}
			if i.button_down(Button::Keyboard(Key::A)) {
				self.character.obey_order(Order::Move(vec2_normalized([-1.0, 0.0])));
			}
			if i.button_down(Button::Keyboard(Key::S)) {
				self.character.obey_order(Order::Move(vec2_normalized([0.0, 1.0])));
			}
			if i.button_down(Button::Keyboard(Key::D)) {
				self.character.obey_order(Order::Move(vec2_normalized([1.0, 0.0])));
			}
		}
	}
}
