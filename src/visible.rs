use piston_window::*;

pub trait Visible {
	fn render(&mut self, args: &RenderArgs, context: &Context, graphics: &mut G2d);
}
