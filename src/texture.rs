use piston_window::*;

use crate::graphical::Graphical;
use crate::visible::Visible;

pub struct Texture {
	// TODO
}

impl Texture {
	pub fn new() -> Self {
		Texture {}
	}
}

impl Graphical for Texture {
	fn draw(&mut self, args: &RenderArgs, context: &Context, graphics: &mut G2d) {
		// TODO
	}
}

impl Visible for Texture {
	fn render(&mut self, args: &RenderArgs, context: &Context, graphics: &mut G2d) {
		self.draw(args, context, graphics);
	}
}
