use vecmath::Vector2;

pub trait Physical {
	fn position(&mut self) -> Vector2<f64>;
}
