use vecmath::Vector2;

pub enum Order {
	Move(Vector2<f64>)
}

pub trait Controllable {
	fn obey_order(&mut self, order: Order);
}