extern crate config;
use find_folder;
use piston_window::*;
use std::path::PathBuf;
use std::str::FromStr;

use crate::character::Character;
use crate::colour::Colour;
use crate::input::Input;
use crate::player::Player;
use crate::temporal::Temporal;
use crate::visible::Visible;

pub struct Game {
	pub input: Input,
	pub window_settings: WindowSettings,
	config: config::Config,
	player: Player<Character>,
	// resources: PathBuf
}

impl Game {
	pub fn new() -> Self {
		let cfg = config::Config::default().merge(config::File::with_name("config.yml")).unwrap().to_owned();
		let window_size = Size::from([cfg.get_int("window.size.width").unwrap_or(1920) as u32, cfg.get_int("window.size.height").unwrap_or(1080) as u32]);
		let window_settings = WindowSettings::new(cfg.get_str("window.title").unwrap_or("zenzenmottenai".to_string()), window_size)
			.exit_on_esc(true)
			.resizable(false);
		let character = Character::new(&window_settings, [0.5, 0.5]);

		Game {
			input: Input::new(),
			window_settings: window_settings,
			config: cfg,
			player: Player::new(character),
			// resources: find_folder::Search::ParentsThenKids(3, 3).for_folder("rsrc").unwrap()
		}
	}

	pub fn render(&mut self, args: &RenderArgs, context: &Context, graphics: &mut G2d) {
		// Clear the screen.
		clear(Colour::from_str(&self.config.get_str("window.clear_colour").unwrap_or("CornflowerBlue".to_string())).unwrap_or(Colour::CornflowerBlue).value(), graphics);

		self.player.get_character().render(args, context, graphics);
	}

	pub fn update(&mut self, args: &UpdateArgs) {
		self.input.update(args, None);
		self.player.update(args, Some(self.input.delta()));
		self.player.get_character().update(args, Some(self.input.delta()));
	}
}
