use std::fmt::{self, Display, Formatter};
use std::str::FromStr;

pub enum Colour {
	Black,
	Blue,
	CornflowerBlue,
	CornflowerGreen,
	CornflowerOrange,
	CornflowerRed,
	CornflowerPurple,
	CornflowerYellow,
	Cyan,
	Green,
	Magenta,
	Red,
	White,
	Yellow
}

impl Colour {
	pub fn to_str(&self) -> &str {
		match self {
			Colour::Black => "Black",
			Colour::Blue => "Blue",
			Colour::CornflowerBlue => "CornflowerBlue",
			Colour::CornflowerGreen => "CornflowerGreen",
			Colour::CornflowerOrange => "CornflowerOrange",
			Colour::CornflowerRed => "CornflowerRed",
			Colour::CornflowerPurple => "CornflowerPurple",
			Colour::CornflowerYellow => "CornflowerYellow",
			Colour::Cyan => "Cyan",
			Colour::Green => "Green",
			Colour::Magenta => "Magenta",
			Colour::Red => "Red",
			Colour::White => "White",
			Colour::Yellow => "Yellow"
		}
	}

	pub fn value(&self) -> [f32; 4] {
		match self {
			Colour::Black => [0.0, 0.0, 0.0, 1.0],
			Colour::Blue => [0.0, 0.0, 1.0, 1.0],
			Colour::CornflowerBlue => [100.0 / 255.0, 149.0 / 255.0, 237.0 / 255.0, 1.0],
			Colour::CornflowerGreen => [100.0 / 255.0, 237.0 / 255.0, 149.0 / 255.0, 1.0],
			Colour::CornflowerOrange => [237.0 / 255.0, 149.0 / 255.0, 100.0 / 255.0, 1.0],
			Colour::CornflowerRed => [237.0 / 255.0, 100.0 / 255.0, 149.0 / 255.0, 1.0],
			Colour::CornflowerPurple => [149.0 / 255.0, 100.0 / 255.0, 237.0 / 255.0, 1.0],
			Colour::CornflowerYellow => [149.0 / 255.0, 237.0 / 255.0, 100.0 / 255.0, 1.0],
			Colour::Cyan => [0.0, 1.0, 1.0, 1.0],
			Colour::Green => [0.0, 1.0, 0.0, 1.0],
			Colour::Magenta => [1.0, 0.0, 1.0, 1.0],
			Colour::Red => [1.0, 0.0, 0.0, 1.0],
			Colour::White => [1.0, 1.0, 1.0, 1.0],
			Colour::Yellow => [1.0, 1.0, 0.0, 1.0]
		}
	}
}

impl FromStr for Colour {
	type Err = ColourError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s {
			"Black" => Ok(Colour::Black),
			"Blue" => Ok(Colour::Blue),
			"CornflowerBlue" => Ok(Colour::CornflowerBlue),
			"CornflowerGreen" => Ok(Colour::CornflowerGreen),
			"CornflowerOrange" => Ok(Colour::CornflowerOrange),
			"CornflowerRed" => Ok(Colour::CornflowerRed),
			"CornflowerPurple" => Ok(Colour::CornflowerPurple),
			"CornflowerYellow" => Ok(Colour::CornflowerYellow),
			"Cyan" => Ok(Colour::Cyan),
			"Green" => Ok(Colour::Green),
			"Magenta" => Ok(Colour::Magenta),
			"Red" => Ok(Colour::Red),
			"White" => Ok(Colour::White),
			"Yellow" => Ok(Colour::Yellow),
			_ => Err(ColourError(String::from(s)))
		}
	}
}

#[derive(Debug)]
pub struct ColourError(String);

impl Display for ColourError {
	fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
		write!(f, "Unsupported colour '{}'", self.0)
	}
}
