use piston_window::*;
use std::collections::HashMap;
use vecmath::{Vector2, vec2_add, vec2_sub};

use crate::temporal::Temporal;

pub struct Input {
	current_state: State,
	previous_state: State
}

impl Input {
	pub fn new() -> Self {
		Input {
			current_state: State::new(),
			previous_state: State:: new()
		}
	}

	pub fn delta(&self) -> Delta {
		Delta {
			current_state: &self.current_state,
			previous_state: &self.previous_state
		}
	}

	pub fn handle(&mut self, args: EventArgs) {
		match args {
			EventArgs::Button(a) => {
				trace!("{:?}", a);
				self.current_state.set_button(a.button, a.state);
			},
			EventArgs::MouseCursor(a) => {
				trace!("{:?}", a);
				self.current_state.position = a;
			},
			EventArgs::MouseScroll(a) => {
				trace!("{:?}", a);
				self.current_state.scroll = vec2_add(self.current_state.scroll, a);
			}
		};
	}
}

impl Temporal for Input {
	fn update(&mut self, _args: &UpdateArgs, _input: Option<Delta>) {
		self.previous_state = self.current_state.clone();
		self.current_state.scroll = [0.0, 0.0];
	}
}

#[derive(Debug)]
pub enum EventArgs {
	Button(ButtonArgs),
	MouseCursor(Vector2<f64>),
	MouseScroll(Vector2<f64>)
}

pub struct Delta<'a> {
	current_state: &'a State,
	previous_state: &'a State
}

impl<'a> Delta<'a> {
	pub fn new(current_state: &'a State, previous_state: &'a State) -> Self {
		Delta {
			current_state: current_state,
			previous_state: previous_state
		}
	}

	pub fn button_up(&self, button: Button) -> bool {
		self.previous_state.get_button(button) == ButtonState::Release && self.current_state.get_button(button) == ButtonState::Release
	}

	pub fn button_down(&self, button: Button) -> bool {
		self.previous_state.get_button(button) == ButtonState::Press && self.current_state.get_button(button) == ButtonState::Press
	}

	pub fn button_pressed(&self, button: Button) -> bool {
		self.previous_state.get_button(button) == ButtonState::Release && self.current_state.get_button(button) == ButtonState::Press
	}

	pub fn button_released(&self, button: Button) -> bool {
		self.previous_state.get_button(button) == ButtonState::Press && self.current_state.get_button(button) == ButtonState::Release
	}

	pub fn position(&self) -> Vector2<f64> {
		self.current_state.position
	}

	pub fn position_delta(&self) -> Vector2<f64> {
		vec2_sub(self.current_state.position, self.previous_state.position)
	}

	pub fn scroll_delta(&self) -> Vector2<f64> {
		self.current_state.scroll
	}
}

pub struct State {
	buttons: HashMap<Button, ButtonState>,
	pub position: Vector2<f64>,
	pub scroll: Vector2<f64>
}

impl State {
	pub fn new() -> Self {
		State {
			buttons: HashMap::new(),
			position: [0.0, 0.0],
			scroll: [0.0, 0.0]
		}
	}

	pub fn get_button(&self, button: Button) -> ButtonState {
		match self.buttons.get(&button) {
			Some(state) => *state,
			_ => ButtonState::Release
		}
	}

	pub fn set_button(&mut self, button: Button, button_state: ButtonState) -> ButtonState {
		trace!("{:?}", self.buttons);
		match self.buttons.insert(button, button_state) {
			Some(state) => state,
			_ => ButtonState::Release
		}
	}
}

impl Clone for State {
	fn clone(&self) -> Self {
		State {
			buttons: self.buttons.clone(),
			position: self.position,
			scroll: self.scroll
		}
	}
}
