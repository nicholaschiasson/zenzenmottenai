use piston_window::*;

pub trait Graphical {
	fn draw(&mut self, args: &RenderArgs, context: &Context, graphics: &mut G2d);
}
