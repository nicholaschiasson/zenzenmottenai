#[macro_use]
extern crate log;

pub mod character;
pub mod colour;
pub mod controllable;
pub mod game;
pub mod graphical;
pub mod input;
pub mod player;
pub mod physical;
pub mod temporal;
pub mod texture;
pub mod visible;

use piston_window::*;

use crate::game::Game;
use crate::input::EventArgs;

fn main() {
	pretty_env_logger::init();

	let opengl = OpenGL::V3_2;

	let mut game = Game::new();

	// Create an Glutin window.
	let mut window: PistonWindow = game.window_settings.clone()
		.graphics_api(opengl)
		.build()
		.unwrap();

	while let Some(e) = window.next() {
		// Button presses (mouse and keyboard)
		e.button(|a| {
			trace!("{:?}", a);
			game.input.handle(EventArgs::Button(a));
		});
		// Mouse absolute position
		e.mouse_cursor(|a| {
			trace!("{:?}", a);
			game.input.handle(EventArgs::MouseCursor(a));
		});
		// Mouse scroll
		e.mouse_scroll(|a| {
			trace!("{:?}", a);
			game.input.handle(EventArgs::MouseScroll(a));
		});
		// Render frame
		e.render(|a| {
			trace!("{:?}", a);
			window.draw_2d(&e, |c, g, _d| {
				game.render(&a, &c, g);
			});
		});
		// Update tick
		e.update(|a| {
			trace!("{:?}", a);
			game.update(&a);
		});
	}
}
