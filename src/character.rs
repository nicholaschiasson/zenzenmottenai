use piston_window::*;
use std::cmp::min;
use vecmath::*;

use crate::colour::Colour;
use crate::controllable::{Controllable, Order};
use crate::input;
use crate::physical::Physical;
use crate::temporal::Temporal;
use crate::texture::Texture;
use crate::visible::Visible;

pub struct Character {
	direction: Vector2<f64>,
	position: Vector2<f64>,
	size: f64,
	speed: f64,
	sprite: Texture,
	velocity: Vector2<f64>
}

impl Character {
	pub fn new(window_settings: &WindowSettings, position: Vector2<f64>) -> Self {
		let window_size = window_settings.get_size();
		Character {
			direction: [0.0, 0.0],
			position: [window_size.width * position[0], window_size.height * position[1]],
			size: min(window_size.width as u64, window_size.height as u64) as f64  / 16.0,
			speed: 250.0,
			sprite: Texture::new(),
			velocity: [0.0, 0.0]
		}
	}
}

impl Controllable for Character {
	fn obey_order(&mut self, order: Order) {
		match order {
			Order::Move(direction) => {
				self.direction = vec2_add(self.direction, vec2_normalized(direction));
			}
		};
	}
}

impl Physical for Character {
	fn position(&mut self) -> Vector2<f64> {
		self.position
	}
}

impl Temporal for Character {
	fn update(&mut self, args: &UpdateArgs, _input: Option<input::Delta>) {
		// Compute final velocity for frame
		let velocity = vec2_add(self.velocity, vec2_scale(if self.direction[0] != 0.0 && self.direction[1] != 0.0 {
			vec2_normalized(self.direction)
		} else {
			self.direction
		}, self.speed * args.dt));

		// Apply velocity
		self.position = vec2_add(self.position, velocity);

		// Reset velocity and movement direction
		self.direction = [0.0, 0.0];
		self.velocity = [0.0, 0.0];
	}
}

impl Visible for Character {
	fn render(&mut self, args: &RenderArgs, context: &Context, graphics: &mut G2d) {
		self.sprite.render(args, context, graphics);

		let square = rectangle::square(0.0, 0.0, self.size);

		let transform = context.transform.trans(self.position[0], self.position[1])
			.trans(-self.size / 2.0, -self.size / 2.0);

		// Draw a box rotating around the middle of the screen.
		rectangle(Colour::Red.value(), square, transform, graphics);
	}
}
